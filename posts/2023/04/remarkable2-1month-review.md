<!--
.. date: 2023-04-27 19:00:00 UTC
.. published: true
.. description: Review after 1 month of ~daily usage of the Remarkable2 digital nonebook
.. tags: remarkable, rm2, notetaking
.. category: geek
.. title: Review of Remarkable2 after 1 month
-->

# Review of Remarkable2 after 1 month

> Used ~daily for almost a month with Staedtler Noris Digital Mini Pen

The software is minimal but effective, my usage patterns are:

## Note taking

I used to write a lot of back-of-the-envelope notes, diagrams, math. Lots of paper, lost, merged, mixed. Now I use RM2 for everything. From school meetings (as a parent) to code reviews.

![Notes about a code review with drawings](/images/code-review-rm2.png)

Do not expect a Google Cursive-like experience, there are no stylus gestures here. Just pick the "select" tool and use it. Given the focused UI of RM2, this is not a real problem. Still, the Cursive UX is one of the best I have tried.

Layers are very useful, there is no feedback on what layer you are in, but copy and undo are here to help. You can draw a grid and reuse it almost like one of the many background templates.

The writing experience is very good, the latency is low. The only problem is the tiny offset between the edge of the pointer and the pixel you are writing on (the thickness of the glass). This kills my arrows, I can't align the tip `<_` with the line. I hope I'll get into absorbing this offset in my muscle memory.

The response of the screen is good, but the closer you are to the edge, the more distortion you get. Just swipe with your fingers to write ~1cm from the edge.

Regarding the virtual tools, I mostly use the fineliner, but I love the Calligraphy pen for headers. The highlighter and colors (blue and red only) are useful and very pleasing only in export. Maybe also in screen sharing, but I haven't tried this feature yet (the official app on my Debian doesn't want to go).

I have not used the OCR feature, just tested it once and it "worked", not exactly as expected because I wrote on two columns and it merged the lines.

## PDF Active Reading

I only used it a few times to read and annotate PDFs, sometimes to proofread my own writing.

You can move the PDF page in all directions, so that you have a virtually infinite margin, so you can really fit the proof of Fermat's theorem on it :P. You can also add new pages to the PDF for more extensive notes.

## Fun with my kids

The "toy" is quite expensive, so they are only allowed to draw under my supervision. They found the UX very intuitive, loved trying out all the pens and drawing stuff in the Quick Sheets (which are pages in a single notebook, but you can later export them to a new titled notebook).

## Resources

- [Awesome reMarkable](https://github.com/reHackable/awesome-reMarkable), not yet hacked anything.

## Conclusion

The toy is expensive, in 1 month I created 8 folders, with 1~6 notebooks each and dropped my paper usage to ~0. I'm able to categorize and tag the notebooks to find them easily. The interface should scale up to dozens of folders and dozens of documents (you can choose whether to see the first page or the current one in the preview), so I hope to be able to find things even when everything is fuller.

Trusting the 100-day trial, I am still considering the purchase. The competitor that has given me the most pause is [PineNote](https://www.pine64.org/pinenote/), only it is still really too immature, although the developments look promising!

I still need to test the after-the-free-trial cloud experience, I fear it will be a sad experience.

> [Comments on Mastodon](https://elk.zone/social.slug.it/@naufraghi/01GZ6A5FHQBE1N1G23EC2ZSVSF)